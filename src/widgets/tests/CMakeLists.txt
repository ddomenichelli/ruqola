# SPDX-FileCopyrightText: 2020-2022 Laurent Montel <montel@kde.org>
# SPDX-License-Identifier: BSD-3-Clause
add_executable(testshowimage_gui testshowimage_gui.cpp)
target_link_libraries(testshowimage_gui libruqolawidgets KF5::I18n)

######
add_executable(testshowvideo_gui testshowvideo_gui.cpp)
target_link_libraries(testshowvideo_gui libruqolawidgets KF5::I18n)

####
add_executable(testaudio_gui testaudio_gui.cpp)
target_link_libraries(testaudio_gui libruqolawidgets KF5::I18n)

####
add_executable(testnotification_gui testnotification_gui.cpp notificationwidget.cpp notificationwidget.h)
target_link_libraries(testnotification_gui libruqolawidgets KF5::I18n)

####
add_executable(testcreatevideomessage_gui testcreatevideomessage_gui.cpp)
target_link_libraries(testcreatevideomessage_gui libruqolawidgets KF5::I18n)
