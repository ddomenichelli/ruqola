/*
   SPDX-FileCopyrightText: 2020-2022 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "textpluginmanagertest.h"
#include <QTest>
QTEST_MAIN(TextPluginManagerTest)

TextPluginManagerTest::TextPluginManagerTest(QObject *parent)
    : QObject(parent)
{
}
