/*
   SPDX-FileCopyrightText: 2020-2022 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "usersinroomlabeltest.h"
#include "room/usersinroomlabel.h"
#include <QTest>
QTEST_MAIN(UsersInRoomLabelTest)

UsersInRoomLabelTest::UsersInRoomLabelTest(QObject *parent)
    : QObject(parent)
{
}
