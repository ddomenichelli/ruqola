/*
   SPDX-FileCopyrightText: 2019-2022 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "messagedownloadmanager.h"

MessageDownloadManager::MessageDownloadManager(QObject *parent)
    : QObject(parent)
{
}

MessageDownloadManager::~MessageDownloadManager() = default;
