/*
   SPDX-FileCopyrightText: 2021-2022 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#pragma once

#include "librocketchatrestapi-qt5_export.h"
#include "restapiabstractjob.h"

#include <QNetworkRequest>
namespace RocketChatRestApi
{
class LIBROCKETCHATRESTAPI_QT5_EXPORT LicensesListJob : public RestApiAbstractJob
{
    Q_OBJECT
public:
    explicit LicensesListJob(QObject *parent = nullptr);
    ~LicensesListJob() override;

    Q_REQUIRED_RESULT bool requireHttpAuthentication() const override;

    Q_REQUIRED_RESULT bool start() override;

    Q_REQUIRED_RESULT QNetworkRequest request() const override;

Q_SIGNALS:
    void licensesListDone(const QJsonObject &obj);

private:
    Q_DISABLE_COPY(LicensesListJob)
    void onGetRequestResponse(const QJsonDocument &replyJson) override;
};
}
